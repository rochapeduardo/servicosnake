﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmTipo.aspx.cs" Inherits="Snake.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Manipulando dados do tipo<br />
            <br />
            Codigo<br />
            <asp:TextBox ID="Txt_Id" runat="server"></asp:TextBox>
            <br />
            Nome<br />
            <asp:TextBox ID="Txt_Nome" runat="server"></asp:TextBox>
            <br />
            Valor<br />
            <asp:TextBox ID="Txt_valor" runat="server"></asp:TextBox>
            <br />
            Tempo<br />
            <asp:TextBox ID="Txt_tempo" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Btn_Create" runat="server" OnClick="Btn_Create_Click" Text="Create" />
            <asp:Button ID="Btn_Read" runat="server" OnClick="Btn_Read_Click" Text="Read" />
            <asp:Button ID="Btn_Update" runat="server" OnClick="Btn_Update_Click" Text="Update" />
            <asp:Button ID="Btn_Delete" runat="server" OnClick="Btn_Delete_Click" Text="Delete" />
            <asp:Button ID="Btn_SearchByName" runat="server" OnClick="Btn_SearchByName_Click" Text="Procurar por nome" />
        </div>
        <asp:GridView ID="GridView" runat="server">
        </asp:GridView>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmJogador.aspx.cs" Inherits="Snake.FrmJogador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 529px; width: 652px;">
            Manipulando Dados do Jogador<br />
            <br />
            <asp:Label ID="lblNome0" runat="server" Text="Codigo"></asp:Label>
            &nbsp;&nbsp;&nbsp;<br />
            <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblNome" runat="server" Text="Nome"></asp:Label>
            <br />
            <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblNick" runat="server" Text="Nickname"></asp:Label>
            <br />
            <asp:TextBox ID="txtNick" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblSenha" runat="server" Text="Senha"></asp:Label>
            <br />
            <asp:TextBox ID="txtSenha" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblSenha0" runat="server" Text="Data Nascimento"></asp:Label>
            <br />
            <asp:TextBox ID="txtDatanasc" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblSenha1" runat="server" Text="E-mail"></asp:Label>
            <br />
            <asp:TextBox ID="txtMail" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="BtnInserir" runat="server" OnClick="BtnInserir_Click" Text="Create" />
            <asp:Button ID="BtnRead" runat="server" OnClick="BtnRead_Click" Text="Read" />
            <asp:Button ID="BtnAlter" runat="server" OnClick="BtnAlter_Click" Text="Update" />
            <asp:Button ID="BtnRemove" runat="server" OnClick="BtnRemove_Click" Text="Delete" />
            <asp:Button ID="BtnConsultarPorNome" runat="server" OnClick="BtnConsultarPorNome_Click" Text="Consultar por nome" />
            <asp:Button ID="BtnConsultarPorNick" runat="server" OnClick="BtnConsultarPorNick_Click" Text="Consultar por nick" />
            <br />
            <br />
            <asp:GridView ID="GridView" runat="server">
            </asp:GridView>
        </div>
    </form>
</body>
</html>

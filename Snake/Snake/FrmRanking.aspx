﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmRanking.aspx.cs" Inherits="Snake.FrmRanking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Dados do ranking<br />
            <br />
            Id do jogador<br />
            <asp:TextBox ID="Txt_IdJogador" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Btn_PlayerRank" runat="server" OnClick="Btn_PlayerRank_Click" Text="Search" />
            <br />
            <br />
            Order By<br />
            <asp:TextBox ID="Txt_OrderBy" runat="server">total</asp:TextBox>
            <br />
            <asp:Button ID="Btn_List" runat="server" OnClick="Btn_List_Click" Text="Order" />
            <br />
            <br />
            <asp:GridView ID="GridView" runat="server">
            </asp:GridView>
        </div>
    </form>
</body>
</html>

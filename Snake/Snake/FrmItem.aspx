﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmItem.aspx.cs" Inherits="Snake.FrmItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Manipulando dados do item<br />
            <br />
            Codigo<br />
            <asp:TextBox ID="Txt_Id" runat="server"></asp:TextBox>
            <br />
            Nome<br />
            <asp:TextBox ID="Txt_Nome" runat="server"></asp:TextBox>
            <br />
            Tipo<br />
            <asp:TextBox ID="Txt_Tipo" runat="server"></asp:TextBox>
            <br />
            Imagem<br />
            <asp:TextBox ID="Txt_Imagem" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Btn_Create" runat="server" OnClick="Btn_Create_Click" Text="Create" />
            <asp:Button ID="Btn_Read" runat="server" OnClick="Btn_Read_Click" Text="Read" />
            <asp:Button ID="Btn_Update" runat="server" OnClick="Btn_Update_Click" Text="Update" />
            <asp:Button ID="Btn_Delete" runat="server" Text="Delete" />
        </div>
    </form>
</body>
</html>

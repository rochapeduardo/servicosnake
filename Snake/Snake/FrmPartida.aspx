﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmPartida.aspx.cs" Inherits="Snake.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Manipulando Dados da partida<br />
            <br />
            Código<br />
            <asp:TextBox ID="Txt_cod" runat="server"></asp:TextBox>
            <br />
            Data de registro<br />
            <asp:TextBox ID="Txt_date" runat="server"></asp:TextBox>
            <br />
            Tempo<br />
            <asp:TextBox ID="Txt_time" runat="server"></asp:TextBox>
            <br />
            Jogador 1<br />
            <asp:TextBox ID="Txt_pl1" runat="server"></asp:TextBox>
            <br />
            Jogador 2<br />
            <asp:TextBox ID="Txt_pl2" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Btn_Create" runat="server" OnClick="Btn_Create_Click" Text="Create" />
            <asp:Button ID="Btn_read" runat="server" OnClick="Btn_read_Click" Text="Read" />
            <asp:Button ID="Btn_update" runat="server" OnClick="Btn_update_Click" Text="Update" />
            <asp:Button ID="Btn_delete" runat="server" OnClick="Btn_delete_Click" Text="Delete" />
            <asp:Button ID="Btn_consByDate" runat="server" OnClick="Btn_consByDate_Click" Text="Consultar por data" />
            <br />
        </div>
        <asp:GridView ID="GridView" runat="server">
        </asp:GridView>
    </form>
</body>
</html>

﻿using SnakeDAO.controller;
using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Snake
{
    public partial class FrmJogador : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtNome.Focus();
        }

        protected void BtnInserir_Click(object sender, EventArgs e)
        {
            txtId.Text = new JogadorController().Create(new Jogador
            {
                Nome = txtNome.Text,
                Nick = txtNick.Text,
                Senha = txtSenha.Text,
                DataNasc = DateTime.Parse(txtDatanasc.Text),
                Mail = txtMail.Text
            }).ToString();
        }

        protected void BtnRead_Click(object sender, EventArgs e)
        {
            JogadorController jogadorController = new JogadorController();
            Jogador jogador = jogadorController.Read(Int32.Parse(txtId.Text));

            txtNome.Text = jogador.Nome;
            txtSenha.Text = jogador.Senha;
            txtNick.Text = jogador.Nick;
            txtMail.Text = jogador.Mail;
            txtDatanasc.Text = jogador.DataNasc.ToString().Split(' ')[0];
        }

        protected void BtnAlter_Click(object sender, EventArgs e)
        {
            JogadorController jogadorController = new JogadorController();

            Jogador jogador = new Jogador
            {
                Id_Jogador = Int32.Parse(txtId.Text),
                Nome = txtNome.Text,
                Nick = txtNick.Text,
                Senha = txtSenha.Text,
                DataNasc = DateTime.Parse(txtDatanasc.Text),
                Mail = txtMail.Text
            };

            jogadorController.Update(jogador);
        }

        protected void BtnRemove_Click(object sender, EventArgs e)
        {
            JogadorController jogadorController = new JogadorController();
            jogadorController.Delete(new Jogador
            {
                Id_Jogador = Int32.Parse(txtId.Text)
            });
        }

        protected void BtnConsultarPorNome_Click(object sender, EventArgs e)
        {
            JogadorController jogadorController = new JogadorController();
            List<Jogador> jogadores = jogadorController.ConsultarByNome(txtNome.Text);

            GridView.DataSource = jogadores;
            GridView.DataBind();
        }

        protected void BtnConsultarPorNick_Click(object sender, EventArgs e)
        {
            JogadorController jogadorController = new JogadorController();
            List<Jogador> jogadores = jogadorController.ConsultarByNick(txtNick.Text);

            GridView.DataSource = jogadores;
            GridView.DataBind();
        }
    }
}
﻿using SnakeDAO.controller;
using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Snake
{
    public partial class FrmHistorico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_Create_Click(object sender, EventArgs e)
        {
            Txt_Id.Text = new HistoricoController().Create(new Historico
            {
                Item = new ItemController().Read(Int32.Parse(Txt_Item.Text)),
                Jogador1 = new JogadorController().Read(Int32.Parse(Txt_pl1.Text)),
                Jogador2 = new JogadorController().Read(Int32.Parse(Txt_pl2.Text)),
                Partida = new PartidaController().Read(Int32.Parse(Txt_Partida.Text)),
            }).ToString();
        }

        protected void Btn_Read_Click(object sender, EventArgs e)
        {
            Historico historico = new HistoricoController().Read(Int32.Parse(Txt_Id.Text));

            Txt_Item.Text = historico.Item.Id_Item.ToString();
            Txt_Partida.Text = historico.Partida.Id_Partida.ToString();

            try
            {
                Txt_pl1.Text = historico.Jogador1.Id_Jogador.ToString();
            }
            catch (Exception)
            {
                Console.WriteLine("Item nao consumido pelo jogador 1");
            }
            try
            {
                Txt_pl2.Text = historico.Jogador2.Id_Jogador.ToString();
            }
            catch (Exception)
            {
                Console.WriteLine("Item nao consumido pelo jogador 2");
            }
        }

        protected void Btn_Update_Click(object sender, EventArgs e)
        {
            new HistoricoController().Update(new Historico
            {
                Id_Historico = Int32.Parse(Txt_Id.Text),
                Item = new ItemController().Read(Int32.Parse(Txt_Item.Text)),
                Jogador1 = new JogadorController().Read(Int32.Parse(Txt_pl1.Text)),
                Jogador2 = new JogadorController().Read(Int32.Parse(Txt_pl2.Text)),
                Partida = new PartidaController().Read(Int32.Parse(Txt_Partida.Text)),
            });
        }

        protected void Btn_Delete_Click(object sender, EventArgs e)
        {
            new HistoricoController().Delete(Int32.Parse(Txt_Id.Text));
        }
    }
}
﻿using SnakeDAO.controller;
using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Snake
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Txt_time.Focus();
        }

        protected void Btn_Create_Click(object sender, EventArgs e)
        {
            Txt_cod.Text = new PartidaController().Create(new Partida
            {
                Tempo = TimeSpan.Parse(Txt_time.Text),
                Jogador1 = new JogadorController().Read(Int32.Parse(Txt_pl1.Text)),
                Jogador2 = new JogadorController().Read(Int32.Parse(Txt_pl2.Text))
            }).ToString();
        }

        protected void Btn_read_Click(object sender, EventArgs e)
        {
            Partida partida = new PartidaController().Read(Int32.Parse(Txt_cod.Text));

            Txt_date.Text = partida.Dataregistro.ToString();
            Txt_time.Text = partida.Tempo.ToString();
            Txt_pl1.Text = partida.Jogador1.Id_Jogador.ToString();
            Txt_pl2.Text = partida.Jogador2.Id_Jogador.ToString();
        }

        protected void Btn_update_Click(object sender, EventArgs e)
        {
            new PartidaController().Update(new Partida
            {
                Dataregistro = DateTime.Parse(Txt_date.Text),
                Tempo = TimeSpan.Parse(Txt_time.Text),
                Jogador1 = new JogadorController().Read(Int32.Parse(Txt_pl1.Text)),
                Jogador2 = new JogadorController().Read(Int32.Parse(Txt_pl2.Text))
            });
        }

        protected void Btn_delete_Click(object sender, EventArgs e)
        {
            new PartidaController().Delete(new Partida
            {
                Id_Partida = Int32.Parse(Txt_cod.Text)
            });
        }

        protected void Btn_consByDate_Click(object sender, EventArgs e)
        {
            List<Partida> partidas = new PartidaController().ConsultarByDate(Txt_date.Text);

            GridView.DataSource = partidas;
            GridView.DataBind();
        }
    }
}
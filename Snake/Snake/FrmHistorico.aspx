﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmHistorico.aspx.cs" Inherits="Snake.FrmHistorico" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Manipulando dados do historico<br />
            <br />
            Codigo<br />
            <asp:TextBox ID="Txt_Id" runat="server"></asp:TextBox>
        </div>
        <div>
            Item<br />
            <asp:TextBox ID="Txt_Item" runat="server"></asp:TextBox>
        </div>
        <div>
            Partida<br />
            <asp:TextBox ID="Txt_Partida" runat="server"></asp:TextBox>
        </div>
        <div>
            Jogador 1<br />
            <asp:TextBox ID="Txt_pl1" runat="server"></asp:TextBox>
        </div>
        <div>
            Jogador 2<br />
            <asp:TextBox ID="Txt_pl2" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Btn_Create" runat="server" OnClick="Btn_Create_Click" Text="Create" />
            <asp:Button ID="Btn_Read" runat="server" OnClick="Btn_Read_Click" Text="Read" />
            <asp:Button ID="Btn_Update" runat="server" OnClick="Btn_Update_Click" Text="Update" />
            <asp:Button ID="Btn_Delete" runat="server" OnClick="Btn_Delete_Click" Text="Delete" />
        </div>
    </form>
</body>
</html>

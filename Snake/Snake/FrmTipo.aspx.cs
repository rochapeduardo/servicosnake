﻿using SnakeDAO.controller;
using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Snake
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_Create_Click(object sender, EventArgs e)
        {
            Txt_Id.Text = new TipoController().Create(new Tipo
            {
                Nome = Txt_Nome.Text,
                Tempo = TimeSpan.Parse(Txt_tempo.Text),
                Valor = Int32.Parse(Txt_valor.Text)
            }).ToString();
        }

        protected void Btn_Read_Click(object sender, EventArgs e)
        {
            Tipo tipo = new TipoController().Read(Int32.Parse(Txt_Id.Text));

            Txt_Nome.Text = tipo.Nome;
            Txt_tempo.Text = tipo.Tempo.ToString();
            Txt_valor.Text = tipo.Valor.ToString();
        }

        protected void Btn_Update_Click(object sender, EventArgs e)
        {
            new TipoController().Update(new Tipo
            {
                Id_Tipo = Int32.Parse(Txt_Id.Text),
                Nome = Txt_Nome.Text,
                Tempo = TimeSpan.Parse(Txt_tempo.Text),
                Valor = Int32.Parse(Txt_valor.Text)
            });
        }

        protected void Btn_Delete_Click(object sender, EventArgs e)
        {
            new TipoController().Delete(new Tipo
            {
                Id_Tipo = Int32.Parse(Txt_Id.Text)
            });
        }

        protected void Btn_SearchByName_Click(object sender, EventArgs e)
        {
            List<Tipo> tipo = new TipoController().ConsultarByNome(Txt_Nome.Text);

            GridView.DataSource = tipo;
            GridView.DataBind();
        }
    }
}
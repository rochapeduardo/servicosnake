﻿using SnakeDAO.controller;
using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Snake
{
    public partial class FrmRanking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_PlayerRank_Click(object sender, EventArgs e)
        {
            GridView.DataSource = new List<Ranking> { new RankingController().Read(Int32.Parse(Txt_IdJogador.Text)) };
            GridView.DataBind();

        }

        protected void Btn_List_Click(object sender, EventArgs e)
        {
            GridView.DataSource = new RankingController().ListBy(Txt_OrderBy.Text);
            GridView.DataBind();
        }
    }
}
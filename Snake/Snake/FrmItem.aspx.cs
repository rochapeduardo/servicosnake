﻿using SnakeDAO.controller;
using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Snake
{
    public partial class FrmItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_Create_Click(object sender, EventArgs e)
        {
            Txt_Id.Text = new ItemController().Create(new Item
            {
                Imagem = Txt_Imagem.Text,
                Nome = Txt_Nome.Text,
                Tipo = new TipoController().Read(Int32.Parse(Txt_Tipo.Text))
            }).ToString();
        }

        protected void Btn_Read_Click(object sender, EventArgs e)
        {
            Item item = new ItemController().Read(Int32.Parse(Txt_Id.Text));

            Txt_Imagem.Text = item.Imagem;
            Txt_Nome.Text = item.Nome;
            Txt_Tipo.Text = item.Tipo.Id_Tipo.ToString();
        }

        protected void Btn_Update_Click(object sender, EventArgs e)
        {
            new ItemController().Update(new Item
            {
                Id_Item = Int32.Parse(Txt_Id.Text),
                Imagem = Txt_Imagem.Text,
                Nome = Txt_Nome.Text,
                Tipo = new TipoController().Read(Int32.Parse(Txt_Tipo.Text))
            });
        }
    }
}
﻿using SnakeDAO.controller;
using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Snake.WS
{
    /// <summary>
    /// Descrição resumida de WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que esse serviço da web seja chamado a partir do script, usando ASP.NET AJAX, remova os comentários da linha a seguir. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        #region Player
        [WebMethod]
        public int Register(Jogador jogador)
        {
            return new JogadorController().Create(new Jogador
            {
                Nick = jogador.Nick,
                Nome = jogador.Nome,
                Mail = jogador.Mail,
                Senha = jogador.Senha,
                DataNasc = jogador.DataNasc
            });
        }

        [WebMethod]
        public Jogador Login(string nick, string senha)
        {
            return new JogadorController().Login(nick, senha);
        } 
        #endregion


        [WebMethod]
        public List<Historico> ListarHistorico(int _partida)
        {
            return new HistoricoController().ConsultarByPartida(new Partida() { Id_Partida = _partida });
        }

        [WebMethod]
        public bool AlterarHistorico(Historico _historico, Jogador _jogador, int _idJ)
        {
            try
            {
                if (_idJ == 0)
                    new HistoricoController().UpdatePl1(_historico.Id_Historico, _jogador.Id_Jogador);
                else if (_idJ== 1)
                    new HistoricoController().UpdatePl2(_historico.Id_Historico, _jogador.Id_Jogador);
        }
            catch
            {
                return false;
            }

            return true;
        }

        [WebMethod]
        public Partida CriarPartida(int _pl1, int _pl2)
        {
            int id = new PartidaController().Create(new Partida()
            {
                Jogador1 = new Jogador() { Id_Jogador = _pl1 },
                Jogador2 = new Jogador() { Id_Jogador = _pl2 },
                Tempo = TimeSpan.Parse("0")
            });

            return new PartidaController().Read(id);
        }

        [WebMethod]
        public bool AlterarTempoPartida(Partida _partida, double _sec)
        {
            try
            {
                Partida temp = new PartidaController().Read(_partida.Id_Partida);

                _partida.Jogador1 = temp.Jogador1;
                _partida.Jogador2 = temp.Jogador2;
                _partida.Tempo = TimeSpan.Parse(_sec.ToString());
                _partida.Dataregistro = temp.Dataregistro;

                new PartidaController().Update(_partida);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}

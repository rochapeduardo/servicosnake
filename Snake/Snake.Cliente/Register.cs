﻿using Snake.Cliente.Services.WSJogador;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake.Cliente
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //if (!(txt_pass.Text == txt_confirmPass.Text))
            //{

            //    return;
            //}

            Jogador jogador = new Jogador()
            {
                Nome = txt_nome.Text,
                Nick = txt_nick.Text,
                Mail = txt_mail.Text,
                Senha = txt_pass.Text
            };
            
            jogador.Id_Jogador = new WSJogador().Register(jogador);

            Hide();
        }
    }
}

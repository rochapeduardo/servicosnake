﻿namespace Snake.Cliente
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.txt_nome = new System.Windows.Forms.TextBox();
            this.txt_nick = new System.Windows.Forms.TextBox();
            this.txt_pass = new System.Windows.Forms.TextBox();
            this.txt_confirmPass = new System.Windows.Forms.TextBox();
            this.txt_datanasc = new System.Windows.Forms.MaskedTextBox();
            this.txt_mail = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // txt_nome
            // 
            this.txt_nome.Location = new System.Drawing.Point(12, 33);
            this.txt_nome.Name = "txt_nome";
            this.txt_nome.Size = new System.Drawing.Size(145, 20);
            this.txt_nome.TabIndex = 2;
            this.txt_nome.Text = "Nome";
            // 
            // txt_nick
            // 
            this.txt_nick.Location = new System.Drawing.Point(12, 59);
            this.txt_nick.Name = "txt_nick";
            this.txt_nick.Size = new System.Drawing.Size(145, 20);
            this.txt_nick.TabIndex = 3;
            this.txt_nick.Text = "Nickname";
            // 
            // txt_pass
            // 
            this.txt_pass.Location = new System.Drawing.Point(12, 85);
            this.txt_pass.Name = "txt_pass";
            this.txt_pass.Size = new System.Drawing.Size(145, 20);
            this.txt_pass.TabIndex = 4;
            this.txt_pass.Text = "Senha";
            // 
            // txt_confirmPass
            // 
            this.txt_confirmPass.Location = new System.Drawing.Point(12, 111);
            this.txt_confirmPass.Name = "txt_confirmPass";
            this.txt_confirmPass.Size = new System.Drawing.Size(145, 20);
            this.txt_confirmPass.TabIndex = 5;
            this.txt_confirmPass.Text = "Senha";
            // 
            // txt_datanasc
            // 
            this.txt_datanasc.Location = new System.Drawing.Point(12, 137);
            this.txt_datanasc.Name = "txt_datanasc";
            this.txt_datanasc.Size = new System.Drawing.Size(145, 20);
            this.txt_datanasc.TabIndex = 8;
            this.txt_datanasc.Text = "Data nascimento";
            // 
            // txt_mail
            // 
            this.txt_mail.Location = new System.Drawing.Point(12, 163);
            this.txt_mail.Name = "txt_mail";
            this.txt_mail.Size = new System.Drawing.Size(145, 20);
            this.txt_mail.TabIndex = 9;
            this.txt_mail.Text = "E-mail";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 189);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Registrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(173, 232);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txt_mail);
            this.Controls.Add(this.txt_datanasc);
            this.Controls.Add(this.txt_confirmPass);
            this.Controls.Add(this.txt_pass);
            this.Controls.Add(this.txt_nick);
            this.Controls.Add(this.txt_nome);
            this.Name = "Register";
            this.ShowInTaskbar = false;
            this.Text = "Register";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox txt_nome;
        private System.Windows.Forms.TextBox txt_nick;
        private System.Windows.Forms.TextBox txt_pass;
        private System.Windows.Forms.TextBox txt_confirmPass;
        private System.Windows.Forms.MaskedTextBox txt_datanasc;
        private System.Windows.Forms.MaskedTextBox txt_mail;
        private System.Windows.Forms.Button button1;
    }
}
﻿using Snake.Cliente.Services.WSJogador;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake.Cliente
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            if (txt_login.Text == "" || txt_login.Text == "")
                return;

            WSJogador wSJogador = new WSJogador();

            try
            {
                Jogador jogador = wSJogador.Login(txt_login.Text, txt_pass.Text);

                //p1.Cod = jogador.Id_Jogador;
                //btnConectar.Enabled = false;
                //txtNick.Enabled = false;
                //txtSenha.Enabled = false;
                //btnRegistro.Enabled = false;
                //conectar();

                new InGame().Show();
                Hide();
            }
            catch (Exception excp)
            { 
                MessageBox.Show(excp.Message);
            }
        }

        private void btn_register_Click(object sender, EventArgs e)
        {
            new Register().ShowDialog();
        }
    }
}

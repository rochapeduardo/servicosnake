﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Snake.Servidor
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener listener = null;
            int porta = 5000;
            GameManager controle = new GameManager();
            bool rodando = true;

            try
            {
                IPAddress enderecoServidor = IPAddress.Parse("127.0.0.1");
                listener = new TcpListener(enderecoServidor, porta);
                listener.Start();

                while (rodando)
                {
                    if (!controle.estaPronto())
                    {
                        Console.WriteLine("Aguardando conexões.");
                        TcpClient cliente = listener.AcceptTcpClient();
                        //controle.adicionaJogador(cliente);
                    }

                }

            }
            catch (SocketException se)
            {
                Console.WriteLine("Erro de rede: {0}", se);
            }
        }
    }
}

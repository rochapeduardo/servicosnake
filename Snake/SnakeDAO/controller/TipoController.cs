﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SnakeDAO.model;

namespace SnakeDAO.controller
{
    public class TipoController : Controller
    {
        public TipoController()
        {
            TableName = "TIPO";
        }

        #region CRUD
        public int Create(Tipo tipo)
        {
            string sql = "INSERT INTO " + TableName + " (nome, valor, tempo) " +
                "VALUES (@nome, @valor, @tempo) SELECT SCOPE_IDENTITY()";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@nome", tipo.Nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@valor", tipo.Valor)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@tempo", tipo.Tempo)
            {
                SqlDbType = System.Data.SqlDbType.Time
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();
            reader.Read();
            String primary_key = reader.GetValue(0).ToString();

            return Int32.Parse(primary_key);
        }

        public Tipo Read(int id)
        {
            String sql = "SELECT * " +
                "FROM " + TableName + " WHERE id_tipo = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            Tipo tipo = null;

            if (reader.Read())
            {
                tipo = new Tipo
                {
                    Id_Tipo = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_tipo")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Valor = Int32.Parse(reader.GetValue(reader.GetOrdinal("valor")).ToString()),
                    Tempo = TimeSpan.Parse(reader.GetValue(reader.GetOrdinal("tempo")).ToString())
                };
            }

            return tipo;
        }

        public void Update(Tipo tipo)
        {
            string sql = "UPDATE " + TableName + " SET nome = @nome, valor = @valor, " +
                "tempo = @tempo where id_tipo = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", tipo.Id_Tipo)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@nome", tipo.Nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@valor", tipo.Valor)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@tempo", tipo.Tempo)
            {
                SqlDbType = System.Data.SqlDbType.Time
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void Delete(Tipo tipo)
        {
            string sql = "DELETE FROM " + TableName + " where id_tipo = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", tipo.Id_Tipo)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        } 
        #endregion

        public List<Tipo> ConsultarByNome(string nome)
        {
            nome += "%";

            String sql = "SELECT * " +
                "FROM " + TableName + " WHERE nome LIKE @nome";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@nome", nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            List<Tipo> tipos = new List<Tipo>();

            while (reader.Read())
            {
                tipos.Add(new Tipo
                {
                    Id_Tipo = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_tipo")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Valor = Int32.Parse(reader.GetValue(reader.GetOrdinal("valor")).ToString()),
                    Tempo = TimeSpan.Parse(reader.GetValue(reader.GetOrdinal("tempo")).ToString())
                });
            }

            return tipos;
        }

        public List<Tipo> Listar()
        {
            String sql = "SELECT * " +
                "FROM " + TableName;

            command = new SqlCommand(sql, Connect());

            reader = command.ExecuteReader();

            List<Tipo> tipos = new List<Tipo>();

            while (reader.Read())
            {
                tipos.Add(new Tipo
                {
                    Id_Tipo = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_tipo")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Valor = Int32.Parse(reader.GetValue(reader.GetOrdinal("valor")).ToString()),
                    Tempo = TimeSpan.Parse(reader.GetValue(reader.GetOrdinal("tempo")).ToString())
                });
            }

            return tipos;
        }
    }
}
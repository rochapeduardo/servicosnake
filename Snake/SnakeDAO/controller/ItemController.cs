﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SnakeDAO.model;

namespace SnakeDAO.controller
{
    public class ItemController : Controller
    {
        public ItemController()
        {
            TableName = "ITEMS";
        }

        #region CRUD
        public int Create(Item _item)
        {
            string sql = "INSERT INTO " + TableName + " (nome, tipo, imagem) " +
                "VALUES (@nome, @tipo, @imagem) SELECT SCOPE_IDENTITY()";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@nome", _item.Nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@tipo", _item.Tipo.Id_Tipo)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@imagem", _item.Imagem)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();
            reader.Read();
            String primary_key = reader.GetValue(0).ToString();

            return Int32.Parse(primary_key);
        }

        public Item Read(int id)
        {
            String sql = "SELECT * " +
                "FROM " + TableName + " WHERE id_item = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            Item item = null;

            if (reader.Read())
            {
                Tipo tipo = new TipoController().Read(Int32.Parse(reader.GetValue(reader.GetOrdinal("tipo")).ToString()));

                item = new Item
                {
                    Id_Item = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_item")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Tipo = tipo,
                    Imagem = reader.GetValue(reader.GetOrdinal("imagem")).ToString()
                };
            }

            return item;
        }

        public void Update(Item _item)
        {
            string sql = "UPDATE " + TableName + " SET nome = @nome, tipo = @tipo, imagem = @imagem " +
                "where id_item = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", _item.Id_Item)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@nome", _item.Nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@tipo", _item.Tipo.Id_Tipo)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@imagem", _item.Imagem)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void Delete(Item _item)
        {
            string sql = "DELETE FROM " + TableName + " where id_item = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", _item.Id_Item)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        } 
        #endregion

        public Item ConsultarByNome(string nome)
        {
            nome += "%";

            String sql = "SELECT * " +
                "FROM " + TableName + " WHERE nome like @nome";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", nome)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            Item item = null;

            if (reader.Read())
            {
                Tipo tipo = new TipoController().Read(Int32.Parse(reader.GetValue(reader.GetOrdinal("tipo")).ToString()));

                item = new Item
                {
                    Id_Item = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_item")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Tipo = tipo,
                    Imagem = reader.GetValue(reader.GetOrdinal("imagem")).ToString()
                };
            }

            return item;
        }

    }
}
﻿using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SnakeDAO.controller
{
    public class RankingController : Controller
    {
        public RankingController()
        {
            TableName = "VIEW_RANKING";
        }

        public Ranking Read(int id_jogador)
        {
            string sql = "SELECT * FROM " + TableName + " WHERE id = @id ";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", id_jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            Ranking ranking;

            if (reader.Read())
            {
                ranking = new Ranking
                {
                    id = Int32.Parse(reader.GetValue(reader.GetOrdinal("id")).ToString()),
                    nick = reader.GetValue(reader.GetOrdinal("nick")).ToString(),
                    total = Int32.Parse(reader.GetValue(reader.GetOrdinal("total")).ToString())
                };

                return ranking;
            }

            return null;
        }
        
        public List<Ranking> ListBy(string ord = "total")
        {
            string sql = "SELECT * FROM " + TableName + " ORDER BY " + ord;

            command = new SqlCommand(sql, Connect());

            reader = command.ExecuteReader();

            List<Ranking> ranking = new List<Ranking>();

            while (reader.Read())
            {
                ranking.Add(new Ranking
                {
                    id = Int32.Parse(reader.GetValue(reader.GetOrdinal("id")).ToString()),
                    nick = reader.GetValue(reader.GetOrdinal("nick")).ToString(),
                    total = Int32.Parse(reader.GetValue(reader.GetOrdinal("total")).ToString())
                });
            }

            return ranking;
        }

        public List<Ranking> Listar()
        {
            string sql = "SELECT * FROM " + TableName;

            command = new SqlCommand(sql, Connect());

            reader = command.ExecuteReader();

            List<Ranking> ranking = new List<Ranking>();

            while (reader.Read())
            {
                ranking.Add(new Ranking
                {
                    id = Int32.Parse(reader.GetValue(reader.GetOrdinal("id")).ToString()),
                    nick = reader.GetValue(reader.GetOrdinal("nick")).ToString(),
                    total = Int32.Parse(reader.GetValue(reader.GetOrdinal("total")).ToString())
                });
            }

            return ranking;
        }
    }
}
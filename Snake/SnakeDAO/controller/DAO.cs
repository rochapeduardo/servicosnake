﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace SnakeDAO.controller
{
    public class DAO
    {
        public SqlParameter parameter;
        public SqlCommand command;
        public SqlDataReader reader;

        public SqlConnection Connect()
        {
            string strConnection = ConfigurationManager.ConnectionStrings["SNAKEDB"].ConnectionString;
            SqlConnection connection = new SqlConnection(strConnection);
            connection.Open();
            return connection;
        }
    }
}
﻿using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SnakeDAO.controller
{
    public class HistoricoController : Controller
    {
        public HistoricoController()
        {
            TableName = "HISTORICO";
        }

        public int Create(Historico historico)
        {
            string sql = "INSERT INTO " + TableName + " (item, partida) " +
                "VALUES(@item, @partida) SELECT SCOPE_IDENTITY()";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@item", historico.Item.Id_Item)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@partida", historico.Partida.Id_Partida)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();
            reader.Read();
            String primary_key = reader.GetValue(0).ToString();

            return Int32.Parse(primary_key);
        }

        public Historico Read(int id)
        {
            String sql = "SELECT * " +
                "FROM " + TableName + " WHERE id_historico = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            Historico historico = null;

            if (reader.Read())
            {
                Item item = new ItemController().Read(
                    Int32.Parse(reader.GetValue(reader.GetOrdinal("item")).ToString()));

                Partida partida = new PartidaController().Read(
                    Int32.Parse(reader.GetValue(reader.GetOrdinal("partida")).ToString()));

                historico = new Historico
                {
                    Id_Historico = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_historico")).ToString()),
                    Item = item,
                    Partida = partida
                };


                try
                {
                    Jogador jogador1 = new JogadorController().Read(
                                            Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador1")).ToString()));

                    historico.Jogador1 = jogador1;
                }
                catch (Exception)
                {
                    Console.WriteLine("Ainda não há registro de itens para o jogador 1");
                }

                try
                {
                    Jogador jogador2 = new JogadorController().Read(
                                    Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador2")).ToString()));

                    historico.Jogador2 = jogador2;
                }
                catch (Exception)
                {
                    Console.WriteLine("Ainda não há registro de itens para o jogador 2");
                }
            }

            return historico;
        }

        public void Update(Historico historico)
        {
            string sql = "UPDATE " + TableName + " SET " +
                "item = @item, partida = @partida, jogador1 = @jogador1, jogador2 = @jogador2 " +
                "where id_historico = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", historico.Id_Historico)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@item", historico.Item.Id_Item)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@partida", historico.Partida.Id_Partida)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador1", historico.Jogador1.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador2", historico.Jogador2.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void UpdatePl1(int _id, int _player)
        {
            string sql = "UPDATE " + TableName + " SET " +
                "jogador1 = @jogador1 " +
                "where id_historico = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", _id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador1", _player)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void UpdatePl2(int _id, int _player)
        {
            string sql = "UPDATE " + TableName + " SET " +
                "jogador2 = @jogador2 " +
                "where id_historico = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", _id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador2", _player)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM " + TableName + " where id_historico = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void UpdatePlayer1(Historico historico)
        {
            string sql = "UPDATE " + TableName + " SET " +
                "jogador1 = @jogador1" +
                "where partida = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", historico.Partida.Id_Partida)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador1", historico.Jogador1.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void UpdatePlayer2(Historico historico)
        {
            string sql = "UPDATE " + TableName + " SET " +
                "jogador2 = @jogador2" +
                "where partida = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", historico.Partida.Id_Partida)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador2", historico.Jogador2.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public List<Historico> ConsultarByPartida(Partida partida)
        {
            string sql = "SELECT * " + TableName + " where partida = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", partida.Id_Partida)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            List<Historico> historico = new List<Historico>();

            if (reader.Read())
            {
                Item item = new ItemController().Read(
                    Int32.Parse(reader.GetValue(reader.GetOrdinal("item")).ToString()));

                Jogador jogador1 = new JogadorController().Read(
                    Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador1")).ToString()));

                Jogador jogador2 = new JogadorController().Read(
                    Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador2")).ToString()));

                historico.Add(new Historico
                {
                    Id_Historico = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_historico")).ToString()),
                    Item = item,
                    Jogador1 = jogador1,
                    Jogador2 = jogador2
                });
            }

            return null;
        }
    }
}

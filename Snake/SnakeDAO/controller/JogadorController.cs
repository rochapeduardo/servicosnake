﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SnakeDAO.Cripto;
using SnakeDAO.model;

namespace SnakeDAO.controller
{
    public class JogadorController : Controller
    {
        public JogadorController()
        {
            TableName = "JOGADOR";
        }

        #region CRUD
        public int Create(Jogador jogador)
        {
            string sql = "INSERT INTO " + TableName + " (nome, nick, senha, datanasc, email) " +
                "VALUES (@nome, @nick, @senha, @datanasc, @email) SELECT SCOPE_IDENTITY()";

            command = new SqlCommand(sql, Connect());

            #region Sql Paramaters
            parameter = new SqlParameter("@nome", jogador.Nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@nick", jogador.Nick)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            jogador.Senha = new Criptografia().GerarHashMd5(jogador.Senha);
            parameter = new SqlParameter("@senha", jogador.Senha)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@datanasc", jogador.DataNasc)
            {
                SqlDbType = System.Data.SqlDbType.Date
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@email", jogador.Mail)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);
            #endregion

            reader = command.ExecuteReader();
            reader.Read();
            String primary_key = reader.GetValue(0).ToString();

            return Int32.Parse(primary_key);
        }

        public Jogador Read(int id)
        {
            String sql = "SELECT * " +
                "FROM " + TableName + " WHERE id_jogador = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            if (reader.Read())
            {
                Jogador jogador = new Jogador
                {
                    Id_Jogador = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_jogador")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Nick = reader.GetValue(reader.GetOrdinal("nick")).ToString(),
                    Senha = reader.GetValue(reader.GetOrdinal("senha")).ToString(),
                    DataNasc = DateTime.Parse(reader.GetValue(reader.GetOrdinal("datanasc")).ToString()),
                    Mail = reader.GetValue(reader.GetOrdinal("email")).ToString()
                };

                return jogador;
            }

            return null;
        }

        public void Update(Jogador jogador)
        {
            string sql = "UPDATE " + TableName + " SET nome = @nome, nick = @nick, senha = @senha, " +
                "datanasc = @datanasc, email = @email where id_jogador = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", jogador.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@nome", jogador.Nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@nick", jogador.Nick)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@senha", jogador.Senha)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@datanasc", jogador.DataNasc)
            {
                SqlDbType = System.Data.SqlDbType.Date
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@email", jogador.Mail)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void Delete(Jogador jogador)
        {
            string sql = "DELETE FROM " + TableName + " where id_jogador = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", jogador.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        } 
        #endregion

        public Jogador Login(string nick, string senha)
        {
            string sql = "SELECT * FROM "+TableName+" WHERE nick = @nick";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@nick", nick)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            Jogador jogador = null;

            if (reader.Read())
            {
                jogador = new Jogador
                {
                    Id_Jogador = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_jogador")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Nick = reader.GetValue(reader.GetOrdinal("nick")).ToString(),
                    Senha = reader.GetValue(reader.GetOrdinal("senha")).ToString(),
                    DataNasc = DateTime.Parse(reader.GetValue(reader.GetOrdinal("datanasc")).ToString()),
                    Mail = reader.GetValue(reader.GetOrdinal("email")).ToString()
                };
            }
            else
            {
                throw new Exception("Usuário não existente");
            }

            //senha = new Criptografia().GerarHashMd5(senha);

            Console.WriteLine(jogador.ToString());

            if (jogador.Senha == senha)
            {
                return jogador;
            }
            else
            {
                string a = "Senha incorreta: " + jogador.Nick + " ---- " + jogador.Senha;
                throw new Exception(a);
            }
        }

        public List<Jogador> ConsultarByNome(string nome)
        {
            nome += "%";

            string sql = "SELECT * FROM " + TableName + " WHERE nome LIKE @nome";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@nome", nome)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            List<Jogador> jogadores = new List<Jogador>();

            while (reader.Read())
            {
                jogadores.Add(new Jogador
                {
                    Id_Jogador = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_jogador")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Nick = reader.GetValue(reader.GetOrdinal("nick")).ToString(),
                    Senha = reader.GetValue(reader.GetOrdinal("senha")).ToString(),
                    DataNasc = DateTime.Parse(reader.GetValue(reader.GetOrdinal("datanasc")).ToString()),
                    Mail = reader.GetValue(reader.GetOrdinal("email")).ToString()
                });
            }

            return jogadores;
        }

        public List<Jogador> ConsultarByNick(string nick)
        {
            nick += "%";

            string sql = "SELECT * FROM " + TableName + " where nick like @nick";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@nick", nick)
            {
                SqlDbType = System.Data.SqlDbType.VarChar
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            List<Jogador> jogadores = new List<Jogador>();

            while (reader.Read())
            {
                jogadores.Add(new Jogador
                {
                    Id_Jogador = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_jogador")).ToString()),
                    Nome = reader.GetValue(reader.GetOrdinal("nome")).ToString(),
                    Nick = reader.GetValue(reader.GetOrdinal("nick")).ToString(),
                    Senha = reader.GetValue(reader.GetOrdinal("senha")).ToString(),
                    DataNasc = DateTime.Parse(reader.GetValue(reader.GetOrdinal("datanasc")).ToString()),
                    Mail = reader.GetValue(reader.GetOrdinal("email")).ToString()
                });
            }

            return jogadores;
        }

    }
}
﻿using SnakeDAO.model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SnakeDAO.controller
{
    public class PartidaController : Controller
    {
        public PartidaController()
        {
            TableName = "PARTIDA";
        }

        #region CRUD
        public int Create(Partida partida)
        {
            string sql = "INSERT INTO " + TableName + " (tempo, jogador1, jogador2) " +
                "VALUES (@tempo, @jogador1, @jogador2) SELECT SCOPE_IDENTITY()";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@tempo", partida.Tempo)
            {
                SqlDbType = System.Data.SqlDbType.Time
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador1", partida.Jogador1.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador2", partida.Jogador2.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();
            reader.Read();
            String primary_key = reader.GetValue(0).ToString();

            return Int32.Parse(primary_key);
        }

        public Partida Read(int id)
        {
            String sql = "SELECT * " +
                "FROM " + TableName + " WHERE id_partida = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", id)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            if (reader.Read())
            {
                Jogador jogador1 = new
                    JogadorController().Read(Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador1")).ToString()));
                Jogador jogador2 = new
                    JogadorController().Read(Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador2")).ToString()));

                Partida partida = new Partida
                {
                    Id_Partida = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_partida")).ToString()),
                    Dataregistro = DateTime.Parse(reader.GetValue(reader.GetOrdinal("dataregistro")).ToString()),
                    Tempo = DateTime.Parse(reader.GetValue(reader.GetOrdinal("tempo")).ToString()).TimeOfDay,
                    Jogador1 = jogador1,
                    Jogador2 = jogador2
                };

                return partida;
            }

            return null;
        }

        public void Update(Partida partida)
        {
            string sql = "UPDATE " + TableName + " SET dataregistro = @dataregistro, tempo = @tempo, " +
                "jogador1 = @jogador1, jogador2 = @jogador2 where id_partida = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id_partida", partida.Id_Partida)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@dataregistro", partida.Dataregistro)
            {
                SqlDbType = System.Data.SqlDbType.SmallDateTime
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@tempo", partida.Tempo)
            {
                SqlDbType = System.Data.SqlDbType.Time
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador1", partida.Jogador1.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@jogador2", partida.Jogador2.Id_Jogador)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        }

        public void Delete(Partida partida)
        {
            string sql = "DELETE FROM " + TableName + " where id_partida = @id";

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@id", partida.Id_Partida)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
        } 
        #endregion

        public List<Partida> ConsultarByDate(string date)
        {
            String sql = "SELECT * FROM PARTIDA WHERE " +
                "dataregistro BETWEEN @data_ini AND @data_end";

            DateTime start = DateTime.Parse(date + " 00:00:00");
            DateTime end = DateTime.Parse(date + " 23:59:59");

            command = new SqlCommand(sql, Connect());

            parameter = new SqlParameter("@data_ini", start)
            {
                SqlDbType = System.Data.SqlDbType.SmallDateTime
            };
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@data_end", end)
            {
                SqlDbType = System.Data.SqlDbType.SmallDateTime
            };
            command.Parameters.Add(parameter);

            reader = command.ExecuteReader();

            List<Partida> partidas = new List<Partida>();

            while (reader.Read())
            {
                Jogador jogador1 = new 
                    JogadorController().Read(Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador1")).ToString()));
                Jogador jogador2 = new 
                    JogadorController().Read(Int32.Parse(reader.GetValue(reader.GetOrdinal("jogador2")).ToString()));

                partidas.Add(new Partida
                {
                    Id_Partida = Int32.Parse(reader.GetValue(reader.GetOrdinal("id_partida")).ToString()),
                    Dataregistro = DateTime.Parse(reader.GetValue(reader.GetOrdinal("dataregistro")).ToString()),
                    Tempo = TimeSpan.Parse(reader.GetValue(reader.GetOrdinal("tempo")).ToString()),
                    Jogador1 = jogador1,
                    Jogador2 = jogador2
                });
            }

            return partidas;
        }
    }
}
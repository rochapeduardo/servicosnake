﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SnakeDAO.model
{
    public class Tipo
    {
        public int Id_Tipo { get; set; }
        public string Nome { get; set; }
        public int Valor { get; set; }
        public TimeSpan Tempo { get; set; }
    }
}
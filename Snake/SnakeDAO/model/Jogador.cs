﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SnakeDAO.model;

namespace SnakeDAO.model
{
    public class Jogador
    {
        public int Id_Jogador { get; set; }
        public string Nome { get; set; }
        public string Nick { get; set; }
        public string Senha { get; set; }
        public DateTime DataNasc { get; set; }
        public string Mail { get; set; }
    }
}
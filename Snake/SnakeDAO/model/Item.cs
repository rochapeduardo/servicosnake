﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SnakeDAO.model
{
    public class Item
    {
        public int Id_Item { get; set; }
        public string Nome { get; set; }
        public Tipo Tipo { get; set; }
        public string Imagem { get; set; }
    }
}
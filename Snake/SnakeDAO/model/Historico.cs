﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SnakeDAO.model
{
    public class Historico
    {
        public int Id_Historico { get; set; }
        public Item Item { get; set; }
        public Partida Partida { get; set; }
        public Jogador Jogador1 { get; set; }
        public Jogador Jogador2 { get; set; }
    }
}
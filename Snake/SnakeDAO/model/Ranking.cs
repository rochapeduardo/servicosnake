﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SnakeDAO.model
{
    public class Ranking
    {
        public int id { get; set; }
        public string nick { get; set; }
        public int total { get; set; }
    }
}
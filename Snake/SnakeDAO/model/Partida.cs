﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SnakeDAO.model
{
    public class Partida
    {
        public int Id_Partida { get; set; }
        public DateTime Dataregistro { get; set; }
        public TimeSpan Tempo { get; set; }
        public Jogador Jogador1 { get; set; }
        public Jogador Jogador2 { get; set; }
    }
}
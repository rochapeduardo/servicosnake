CREATE TRIGGER T_PARTIDA_HISTORICO
ON PARTIDA 
FOR INSERT
AS
BEGIN TRAN

DECLARE @COD_PARTIDA INT;
DECLARE @CONT INT;

SELECT @COD_PARTIDA = id_partida FROM INSERTED;
SET @CONT = 0;

WHILE (@CONT < 10)
    BEGIN
        INSERT INTO HISTORICO (ITEM, PARTIDA) 
        VALUES ((SELECT TOP(1) id_item FROM ITEMS ORDER BY NEWID()), @COD_PARTIDA);
        SET @CONT = @CONT + 1;
    END
COMMIT TRAN
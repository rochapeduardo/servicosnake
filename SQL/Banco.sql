create database SNAKEDB;

use SNAKEDB;

create table JOGADOR(
id_jogador int primary key identity ( 1, 1),
nome varchar(100) not null,
nick varchar (100) not null unique,
senha varchar (100) not null,
datanasc date null,
email varchar(100) not null unique
);

create table PARTIDA (
id_partida int primary key identity ( 1, 1),
dataregistro smalldatetime default getdate(),
tempo time null,
jogador1 int,
jogador2 int,
constraint fk_partida_jogador1 foreign key (jogador1) references JOGADOR (id_jogador),
constraint fk_partida_jogador2 foreign key (jogador2) references JOGADOR (id_jogador)
);

create table TIPO
(
id_tipo int primary key identity ( 1, 1),
nome varchar(100),
valor int,
tempo time
);

create table ITEMS
(
id_item int primary key identity ( 1, 1),
nome varchar(40) not null,
tipo int not null,
imagem varchar(60),
constraint fk_itens_tipo foreign key (tipo) references TIPO (id_tipo)
);

create table HISTORICO(
id_historico int primary key identity ( 1, 1),
item int not null,
partida int not null,
jogador1 int null,
jogador2 int null,
constraint fk_historico_item foreign key (item) references ITEMS (id_item),
constraint fk_historico_partida foreign key (partida) references PARTIDA (id_partida)
);

select * from JOGADOR;
select * from TIPO;
select * from ITEMS;
select * from PARTIDA;
select * from HISTORICO;